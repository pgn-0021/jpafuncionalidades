package br.edu.estacio.principal;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.edu.estacio.model.Pessoa;

public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("Compras");
		EntityManager em = emf.createEntityManager();
		
		//Coloque o seu código aqui!!
		Pessoa maria = new Pessoa();
		maria.setNome("Maria");
		maria.setSobrenome("Joana");
		
		Pessoa joao = new Pessoa();
		joao.setNome("João");
		joao.setSobrenome("Henrique");
		
		Pessoa antonio = new Pessoa();
		antonio.setNome("Antonio");
		antonio.setSobrenome("Gomes");
		
		em.getTransaction().begin();
		em.persist(maria);
		em.persist(joao);
		em.persist(antonio);
		em.getTransaction().commit();
				
		//Buscando objetos pelo classe
		em.getTransaction().begin();
		Pessoa pf = em.find(Pessoa.class, 2); //Chave primária (id)
		System.out.println("Pessoa de codigo '2' "+pf.getNome());
		em.getTransaction().commit();
		
		//Atualizando o objeto
		em.getTransaction().begin();
		Pessoa a = em.find(Pessoa.class, 3);
		a.setSobrenome("Silva");
		em.persist(a);
		em.getTransaction().commit();
		
		//Pesquisando um único nome
		String result = (String) em.createNativeQuery("Select nome from pessoa where nome='Maria'").getSingleResult();
		System.out.println("Nome selecionado é: "+result);
		
		//Padrão do hibernate/jpa é createQuery trabalhando com ORM
		
		//Relacionando todas as pessoas com TypedQuery ORM
		TypedQuery<Pessoa> query = em.createQuery("Select p from Pessoa p",Pessoa.class);
		List<Pessoa> pessoas = query.getResultList();
		for (Pessoa p : pessoas) {
			System.out.println("Nome: "+p.getNome());
		}
		
		//Ou fazer a mesma consulta com
		Query query1 = em.createQuery("Select p from Pessoa p", Pessoa.class);
		List<Pessoa> pessoas1 = query1.getResultList();
		for (Pessoa p: pessoas1) {
			System.out.println("Nome: "+p.getNome());
		}
				
		//Excluindo um objeto
		em.getTransaction().begin();
		Pessoa d = em.find(Pessoa.class, 1);
		em.remove(d);
		em.getTransaction().commit();
		
		em.close();
		emf.close();
	}

}
